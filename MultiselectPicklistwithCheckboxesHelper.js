({
	addPicklistValues : function(component,event,selectedValues){
        var action = component.get("c.saveSelectedValues");
        var recordId = component.get("v.recordId");
        var sfObject = component.get("v.sfObject");
        var customField = component.get("v.customField");
        console.log('FROM-HELPER==> '+recordId);
        action.setParams({
            "recordId":recordId,
            "sfObj":sfObject,
            "field":customField,
            "selectedValues":JSON.stringify(selectedValues)
        });
        action.setCallback(this, function(Result) {
            console.log('Check Result State==> '+Result.getState());
            if(Result.getState() === "SUCCESS"){
                
                var msg=Result.getReturnValue();
                console.log('THIS_REFRENCE==>'+ JSON.stringify(this));
                var showToast = $A.get("e.force:showToast"); 
        		showToast.setParams({ 
            		"message" : msg,
            		"type" : "success",
            		"mode" : "dismissible"
        		}); 
        		showToast.fire();    
            }         
        });
        $A.enqueueAction(action);
    }
})